# Balancereye ui
### Description
TODO
## Installing Essentials
* Install [Nodejs](https://nodejs.org/en/download/)
    * Download 'Current' Version
* Install [Yarn](https://yarnpkg.com/en/docs/install) 


## Project Setup
* Install all dependencies: ``yarn install``
* Build the project: ``yarn build``
* Start developing ``yarn start``

### Webstorm
* In settings, make sure to enable node code assistance.
* Swap dependency manager from NPM to Yarn