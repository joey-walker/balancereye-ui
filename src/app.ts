import 'bootstrap/dist/css/bootstrap.min.css';
import Vue from 'vue';
import VueRouter from 'vue-router';
import * as buttonCounter from './components/test';
import router from './routes';

// if ('serviceWorker' in navigator) {
//     window.addEventListener('load', () => {
//         navigator.serviceWorker.register('/service-worker.js').then(registration => {
//             console.log('SW registered: ', registration);
//         }).catch(registrationError => {
//             console.log('SW registration failed: ', registrationError);
//         });
//     });
// }

Vue.use(VueRouter);

const app = new Vue({
    router,
    data: {
        appName: 'Balancers Eye',
        components: {
            buttonCounter
        }
    }
}).$mount('#app');

