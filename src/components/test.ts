import Vue from 'vue';

Vue.component('buttonCounter', {
    data: () => {
        return {
            count: 0
        };
    },
    template: '<button class="col-md-12" v-on:click="count++">You clicked me {{ count }} times. Hi!</button>'
});
