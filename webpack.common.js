'use strict';
const {VueLoaderPlugin} = require('vue-loader');
const {HashedModuleIdsPlugin, HotModuleReplacementPlugin} = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const WorkboxWebpackPlugin = require('workbox-webpack-plugin');
const path = require('path');

module.exports = {
    entry: {
        app: './src/app.ts'
    },
    target: "web",
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: "all",
                }
            }
        },
        runtimeChunk: 'single'
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        },
        extensions: ['.ts', '.tsx', '.js']
    },
    module: {
        rules: [
            //typescript and vue components
            {
                test: /\.tsx?$/,
                exclude: /node_modules|vue\/src/,
                loader: "ts-loader",
                options: {
                    appendTsSuffixTo: [/\.vue$/]
                }
            },
            //pure components in vue
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            // stylus loader
            {
                test: /\.styl(us)?$/,
                use: ['stylus-loader', 'vue-style-loader', 'css-loader']
            },
            //css loader for third party css
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            //images
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ['file-loader']
            },
            //fonts
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: ['file-loader']
            },
            {
                test: /\.html$/,
                use: ['html-loader']
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            title: 'Balancer\'s eye',
            inject: true
        }),
        new HashedModuleIdsPlugin(),
        new HotModuleReplacementPlugin(),
        new WorkboxWebpackPlugin.GenerateSW({
            // these options encourage the ServiceWorkers to get in there fast
            // and not allow any straggling "old" SWs to hang around
            clientsClaim: true,
            skipWaiting: true
        })
    ],
    output: {
        filename: "[name].[hash].bundle.js",
        path: path.resolve(__dirname, 'dist')
    },
};